package com.example.mobileencryption.utils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.math.BigInteger;
import java.util.Arrays;

public class Utils {
    public static boolean isHexString(String hexString){
        try {
            stringHexToByte(hexString);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static String byteToStringHex(byte[] bytes) {
        return Hex.encodeHexString(bytes);
//        return new BigInteger(1, bytes).toString(16);
    }

    public static byte[] stringHexToByte(String hexString) throws DecoderException {
        if (hexString.length() % 2 != 0) hexString= "0".concat(hexString);
        return Hex.decodeHex(hexString);
//        byte[] byteArray = new BigInteger(hexString, 16)
//                .toByteArray();
//        if (byteArray[0] == 0) {
//            byte[] output = new byte[byteArray.length - 1];
//            System.arraycopy(
//                    byteArray, 1, output,
//                    0, output.length);
//            return output;
//        }
//        return byteArray;
    }

    public static byte[] get32BitOf256Bit(byte[] data){
//        return Arrays.copyOfRange(data, data.length - 4, data.length);

        int N=31;
        return new BigInteger(data)
                .and(
                        BigInteger.ZERO.setBit(N).subtract(BigInteger.ONE)
                ).toByteArray();
    }

    public static BigInteger getLast32BitToBigInteger(byte[] data){
        int N=31;
        return new BigInteger(data)
                .or(
                        BigInteger.ZERO.setBit(N).subtract(BigInteger.ONE)
                );
    }
}
