package com.example.mobileencryption;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.mobileencryption.utils.Utils;

public class EncryptTextActivity extends AppCompatActivity {

    EditText inputData, inputKey, outputResult;
    Button encryptButton, decryptButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encrypt_text);
        inputData = findViewById(R.id.dataText);
        inputKey = findViewById(R.id.key);
        outputResult = findViewById(R.id.result);

        encryptButton = findViewById(R.id.encryptButton);
        decryptButton = findViewById(R.id.decryptButton);

        encryptButton.setOnClickListener(view -> {
            byte[] output =
                    new byte[0];

            String input = String.valueOf(inputData.getText());
            System.out.println(Utils.isHexString(input));
            if(Utils.isHexString(input)){
                try {
                    output = Utils.stringHexToByte(input);
                    System.out.println(Utils.byteToStringHex(output));
                    outputResult.setText(Utils.byteToStringHex(output));
                } catch (Exception e) {
                    e.printStackTrace();
                }




            }else {
                outputResult.setText("Not Hex Char");
            }



        });
    }
}