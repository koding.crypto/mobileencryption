package com.example.mobileencryption;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import com.example.mobileencryption.crypto.HashSHA3;
import com.google.android.material.navigation.NavigationView;

import org.bouncycastle.util.encoders.Hex;

import java.math.BigInteger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);

        findViewById(R.id.imageMenu).setOnClickListener(view -> {
            drawerLayout.openDrawer(GravityCompat.START);
        });

        NavigationView navigationView = findViewById(R.id.navigationView);
        navigationView.setItemIconTintList(null);

        NavController navController = Navigation.findNavController(this, R.id.navHostFragment);
        NavigationUI.setupWithNavController(navigationView, navController);





//        String freetext = "abc";
//        String hexatext = "uji coba text";
//
//        HashSHA3 hashSHA3 = new HashSHA3(256);
//        String INPUT = freetext;
//        System.out.println("FOR ENCRYPT BUTTON");
//        System.out.println("HASH:");
////        byte[] result = hashSHA3.hashString(INPUT, null);
////        System.out.println(Hex.toHexString(result));
////        System.out.println(new BigInteger(result).toString());
//        System.out.println();

    }

    public void toEcnryptFileActivity(View view) {
        Intent intent = new Intent(MainActivity.this, EncryptFileActivity.class);
        startActivity(intent);
    }

    public void toEcnryptTextActivity(View view) {
        Intent intent = new Intent(MainActivity.this, EncryptTextActivity.class);
        startActivity(intent);
    }
}