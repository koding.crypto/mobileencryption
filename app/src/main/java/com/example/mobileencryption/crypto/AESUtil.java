package com.example.mobileencryption.crypto;

import com.example.mobileencryption.utils.Utils;
import org.apache.commons.codec.DecoderException;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Hex;

public class AESUtil {
    BufferedBlockCipher engine = null;

    byte[] key = null;
    byte[] IV = null; // The initialization vector needed by the CBC mode

    int size1, size2;

    public AESUtil(byte[] key, byte[]iv) {
        this.key = new byte[key.length];
        System.arraycopy(key, 0 , this.key, 0, key.length);

        IV = new byte[iv.length];
        System.arraycopy(iv, 0 , this.IV, 0, iv.length);
    }

    public AESUtil(byte[] key) throws DecoderException {
        this.key = new byte[key.length];
        System.arraycopy(key, 0 , this.key, 0, key.length);

        IV = Utils.stringHexToByte("00000000000000000000000000000000");
    }

    public void InitCiphers(){
        engine = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESEngine()));
    }

    public byte[] encrypt(byte[] data) throws InvalidCipherTextException {
        engine.init(true, new ParametersWithIV(new KeyParameter(key), IV));
        byte[] enc = new byte[engine.getOutputSize(data.length)];
        size1 = engine.processBytes(data, 0,data.length, enc, 0);
        size2 = engine.doFinal(enc, size1);

        byte[] encryptedContent = new byte[size1 + size2];
        System.arraycopy(enc, 0, encryptedContent, 0, encryptedContent.length);
        return encryptedContent;
    }

    public byte[] decrypt(byte[] data) throws InvalidCipherTextException {
        engine.init(false, new ParametersWithIV(new KeyParameter(key), IV));
        byte[] dec = new byte[engine.getOutputSize(data.length)];
        size1 = engine.processBytes(data, 0, data.length, dec, 0);
        size2 = engine.doFinal(dec, size1);

        byte[] decryptedContent = new byte[size1 + size2];
        System.arraycopy(dec, 0, decryptedContent, 0, decryptedContent.length);
        return decryptedContent;
    }
}
