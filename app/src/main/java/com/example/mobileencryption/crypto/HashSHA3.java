package com.example.mobileencryption.crypto;

import org.bouncycastle.jcajce.provider.digest.SHA3.DigestSHA3;

public class HashSHA3 {
    int bitLength;
    DigestSHA3 digestSHA3;

    public HashSHA3(int bitLength) {
        this.bitLength = bitLength;
        this.digestSHA3 = new DigestSHA3(bitLength);
    }

    public byte[] hashByte(byte[] input, DigestSHA3 digestSHA3) {
        this.digestSHA3 = (digestSHA3 == null) ? this.digestSHA3 : digestSHA3;
        this.digestSHA3.update(input);
        return this.digestSHA3.digest();
    }

    public byte[] hashString(String input, DigestSHA3 digestSHA3) {
        return hashByte(input.getBytes(), digestSHA3);
    }
}
