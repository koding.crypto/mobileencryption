package com.example.mobileencryption.crypto;

import com.example.mobileencryption.utils.Utils;
import org.apache.commons.codec.DecoderException;
import org.bouncycastle.crypto.InvalidCipherTextException;

import java.math.BigInteger;

public class MyAlgo {
    byte[] keyByte = new byte[0];
    byte[] dataByte = new byte[0];
    //    String dataHex;
    byte[] hashByte = new byte[0];
    byte[] hashAttachedByte = new byte[32];
    byte[] hashLastByte = new byte[0];
    byte[] preRotationByte = new byte[0];
    String preRotationHex;
    byte[] concatedDataByte = new byte[0];
    byte[] leftRotatedByte = new byte[0];

    int numberOfRotation;

    HashSHA3 hashSHA3;
    AESUtil aesUtil;
    ShiftBytes shiftBytes;
    BigInteger leftRotated, rightRotated;

    public MyAlgo(byte[] keyByte) {
        this.keyByte = keyByte;
    }

    public void encrypt(byte[] dataByte) throws DecoderException, InvalidCipherTextException {
        this.dataByte = dataByte;
//        Hashing Data
        hashSHA3 = new HashSHA3(256);
        hashByte = hashSHA3.hashByte(dataByte, null);
        hashLastByte = Utils.get32BitOf256Bit(hashByte);
//        System.out.println("Hashing");
//        System.out.println(Utils.byteToStringHex(hashByte));
//        System.out.println(Utils.byteToStringHex(hashLastByte));

//        Encrypting
        aesUtil = new AESUtil(keyByte);
        aesUtil.InitCiphers();
        preRotationByte = aesUtil.encrypt(dataByte);
        preRotationHex = Utils.byteToStringHex(preRotationByte);
//        System.out.println("Encrypted Result");
//        System.out.println(Utils.byteToStringHex(preRotationByte));
//        System.out.println("in BigInte");
//        System.out.println(new BigInteger(1, preRotationByte).toString(16));
//        System.out.println();
//        Rotate Encrypted Data
//        System.out.println("Hash Last Byte "+(new BigInteger(1, hashLastByte)));
//        System.out.println("Hash Last Byte "+(new BigInteger(1, hashLastByte).bitLength()));
//        System.out.println("Hash Last Byte Hex "+(new BigInteger(1, hashLastByte).toString(16)));
//        System.out.println("Hash Last Byte int "+(new Integer(hashLastByte)));
        numberOfRotation = new BigInteger(1, hashLastByte).intValue();
        shiftBytes = new ShiftBytes();
//        System.out.println("Num Rotation "+numberOfRotation);
//        System.out.println(shiftBytes.cyclicLeftShift(new BigInteger(preRotationHex, 16), (preRotationByte.length*8), numberOfRotation).toString(16));
        leftRotated = shiftBytes.cyclicLeftShift(new BigInteger(preRotationHex, 16), (preRotationByte.length*8), numberOfRotation);
//        System.out.println("selip");
//        System.out.println(leftRotated.toString(16));
        leftRotatedByte = Utils.stringHexToByte(
                leftRotated.toString(16)
        );
//        System.out.println("Rotate Encrypted Data");
//        System.out.println(leftRotated.bitLength());
//        System.out.println(leftRotated.toByteArray().length);
//        System.out.println(numberOfRotation);
//        System.out.println("Utils.byteToStringHex");
//        System.out.println(Utils.byteToStringHex(leftRotatedByte));
//        System.out.println("BigInteger .toString(16)");
//        System.out.println(leftRotated.toString(16));
//        System.out.println(new BigInteger(1, leftRotatedByte).toString(16));

//        Concate Data
        concatedDataByte = new byte[leftRotatedByte.length + hashByte.length];
        System.arraycopy(hashByte, 0, concatedDataByte, 0, hashByte.length);
        System.arraycopy(leftRotatedByte, 0, concatedDataByte, hashByte.length, leftRotatedByte.length);
//        System.out.println("Concate");
//        System.out.println(hashByte.length);
//        System.out.println(leftRotatedByte.length);
//        System.out.println(concatedDataByte.length);
//        System.out.println(Utils.byteToStringHex(concatedDataByte));
    }

    public void decrypt(byte[] concatedDataByte) throws DecoderException, InvalidCipherTextException {
        this.concatedDataByte = concatedDataByte;

//        Split Data
        leftRotatedByte = new byte[concatedDataByte.length-32];
        System.arraycopy(concatedDataByte, 0, hashAttachedByte, 0, 32);
        System.arraycopy(concatedDataByte, 32, leftRotatedByte, 0, concatedDataByte.length-32);
        leftRotated = new BigInteger(1, leftRotatedByte);
        preRotationHex = Utils.byteToStringHex(leftRotatedByte);
//        System.out.println("Split Data");
//        System.out.println(concatedDataByte.length);
//        System.out.println(Utils.byteToStringHex(concatedDataByte));
//        System.out.println(hashAttachedByte.length);
//        System.out.println(Utils.byteToStringHex(hashAttachedByte));
//        System.out.println(leftRotatedByte.length);
//        System.out.println(Utils.byteToStringHex(leftRotatedByte));

//TESTING START
//        shiftBytes = new ShiftBytes();
//        String hexString = "5872083e964e81771ea32e570c81dfe8";
//        byte[] data = Utils.stringHexToByte(
//                hexString);
//        BigInteger input = new BigInteger(hexString, 16);
//        BigInteger result = shiftBytes.cyclicRightShift(input, (data.length*8), 8);
//        System.out.println();
//        System.out.println("Right Rotated");
//        System.out.println(result.toString(16));
//        TESTING END
//        Rotate Encrypted Data
        hashLastByte = Utils.get32BitOf256Bit(hashAttachedByte);
        numberOfRotation = new BigInteger(1, hashLastByte).intValue();
        shiftBytes = new ShiftBytes();
        rightRotated = shiftBytes.cyclicRightShift(new BigInteger(preRotationHex, 16), (leftRotatedByte.length*8), numberOfRotation);
//        System.out.println("Panjang awal "+preRotationByte.length);
//        preRotationByte = rightRotated.toByteArray();
        preRotationByte = Utils.stringHexToByte(
                rightRotated.toString(16)
        );
//        System.out.println("BitLength "+leftRotatedByte.length+" = "+leftRotatedByte.length*8);
//        System.out.println("Rotated Encrypted Data");
//        System.out.println(rightRotated.toString(16));
//        System.out.println(numberOfRotation);
//        System.out.println(rightRotated.bitLength());
//        System.out.println(rightRotated.toByteArray().length);
//        System.out.println(preRotationByte.length);
//        System.out.println(Utils.byteToStringHex(preRotationByte));
//                System.out.println(rightRotated.toString(16));
//
//        Decrypting
        aesUtil = new AESUtil(keyByte);
        aesUtil.InitCiphers();
        this.dataByte = aesUtil.decrypt(preRotationByte);
//        System.out.println("Decrypted Result");
//        System.out.println(Utils.byteToStringHex(this.dataByte));

//        Hashing Decrypted Data
        hashSHA3 = new HashSHA3(256);
        hashByte = hashSHA3.hashByte(this.dataByte, null);
        hashLastByte = Utils.get32BitOf256Bit(hashByte);
//        System.out.println("Hashing");
//        System.out.println(Utils.byteToStringHex(hashByte));
//        System.out.println(Utils.byteToStringHex(hashLastByte));



    }

    public byte[] getKeyByte() {
        return keyByte;
    }

    public void setKeyByte(byte[] keyByte) {
        this.keyByte = keyByte;
    }

    public byte[] getDataByte() {
        return dataByte;
    }

    public void setDataByte(byte[] dataByte) {
        this.dataByte = dataByte;
    }

    public byte[] getHashByte() {
        return hashByte;
    }

    public void setHashByte(byte[] hashByte) {
        this.hashByte = hashByte;
    }

    public byte[] getHashAttachedByte() {
        return hashAttachedByte;
    }

    public void setHashAttachedByte(byte[] hashAttachedByte) {
        this.hashAttachedByte = hashAttachedByte;
    }

    public byte[] getHashLastByte() {
        return hashLastByte;
    }

    public void setHashLastByte(byte[] hashLastByte) {
        this.hashLastByte = hashLastByte;
    }

    public byte[] getPreRotationByte() {
        return preRotationByte;
    }

    public void setPreRotationByte(byte[] preRotationByte) {
        this.preRotationByte = preRotationByte;
    }

    public String getPreRotationHex() {
        return preRotationHex;
    }

    public void setPreRotationHex(String preRotationHex) {
        this.preRotationHex = preRotationHex;
    }

    public byte[] getConcatedDataByte() {
        return concatedDataByte;
    }

    public void setConcatedDataByte(byte[] concatedDataByte) {
        this.concatedDataByte = concatedDataByte;
    }

    public byte[] getLeftRotatedByte() {
        return leftRotatedByte;
    }

    public void setLeftRotatedByte(byte[] leftRotatedByte) {
        this.leftRotatedByte = leftRotatedByte;
    }

    public int getNumberOfRotation() {
        return numberOfRotation;
    }

    public void setNumberOfRotation(int numberOfRotation) {
        this.numberOfRotation = numberOfRotation;
    }

    public HashSHA3 getHashSHA3() {
        return hashSHA3;
    }

    public void setHashSHA3(HashSHA3 hashSHA3) {
        this.hashSHA3 = hashSHA3;
    }

    public AESUtil getAesUtil() {
        return aesUtil;
    }

    public void setAesUtil(AESUtil aesUtil) {
        this.aesUtil = aesUtil;
    }

    public ShiftBytes getShiftBytes() {
        return shiftBytes;
    }

    public void setShiftBytes(ShiftBytes shiftBytes) {
        this.shiftBytes = shiftBytes;
    }

    public BigInteger getLeftRotated() {
        return leftRotated;
    }

    public void setLeftRotated(BigInteger leftRotated) {
        this.leftRotated = leftRotated;
    }

    public BigInteger getRightRotated() {
        return rightRotated;
    }

    public void setRightRotated(BigInteger rightRotated) {
        this.rightRotated = rightRotated;
    }
}
/*
 Hashing
5df9f1b761895b11d2ac02d4312dcb25872faa2bf811eb0b34c5b5b263e40cb6
63e40cb6
Encrypted Result
e85872083e964e81771ea32e570c81df
00e85872083e964e81771ea32e570c81df
in BigInte
e85872083e964e81771ea32e570c81df

Rotate Encrypted Data
1675889846
Utils.byteToStringHex
5872083e964e81771ea32e570c81dfe8
BigInteger .toString(16)
5872083e964e81771ea32e570c81dfe8
5872083e964e81771ea32e570c81dfe8
Concate
32
16
48
5df9f1b761895b11d2ac02d4312dcb25872faa2bf811eb0b34c5b5b263e40cb65872083e964e81771ea32e570c81dfe8




DECRYPT


Split Data
49
5df9f1b761895b11d2ac02d4312dcb25872faa2bf811eb0b34c5b5b263e40cb600a05dc7a8cb95c32077fa161c820fa593
32
5df9f1b761895b11d2ac02d4312dcb25872faa2bf811eb0b34c5b5b263e40cb6
17
00a05dc7a8cb95c32077fa161c820fa593
Rotated Encrypted Data
1675889846
00dfe85872083e964c0281771ea32e570c81
 */