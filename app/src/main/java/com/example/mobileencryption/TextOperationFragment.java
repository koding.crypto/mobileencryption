package com.example.mobileencryption;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.mobileencryption.crypto.AESUtil;
import com.example.mobileencryption.crypto.HashSHA3;
import com.example.mobileencryption.crypto.MyAlgo;
import com.example.mobileencryption.crypto.ShiftBytes;
import com.example.mobileencryption.utils.Utils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.InvalidCipherTextException;

import java.math.BigInteger;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TextOperationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TextOperationFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final String TAG = "EncryptText";
    Button encryptButton, decryptButton;
    EditText dataText, key, hash, hashInt, resultPreRotation, result;

    public TextOperationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TextOperationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TextOperationFragment newInstance(String param1, String param2) {
        TextOperationFragment fragment = new TextOperationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_text_operation, container, false);

        View views = inflater.inflate(R.layout.fragment_text_operation, container, false);
        encryptButton = views.findViewById(R.id.encryptButton);
        decryptButton = views.findViewById(R.id.decryptButton);

        dataText = views.findViewById(R.id.dataText);
        key = views.findViewById(R.id.key);
        hash =views.findViewById(R.id.hash);
        hashInt = views.findViewById(R.id.hashInt);
        resultPreRotation = views.findViewById(R.id.resultPreRotation);
        result = views.findViewById(R.id.result);

//        dataText.setText("5df9f1b761895b11d2ac02d4312dcb25872faa2bf811eb0b34c5b5b263e40cb600fffffffffffffffffffa161c820fa593");

        //Encrypted
        dataText.setText("cc466e53cd94a057cef0db90d4d5dfc8eceaa4bd55b48e866ab4aba7806dfd8f91bd999017e463ca95631f288a1462508a4572a8dc42de71424fdee83918945eb57510e59f9d416756f9669cba48174510de2bcfd8e18300ffc8b9dbd3629c5");
        key.setText("9dc2c84a37850c11699818605f47958c");





        encryptButton.setOnClickListener(view -> {
            byte[] keyByte = new byte[0];
            byte[] dataByte = new byte[0];
//            byte[] hashByte = new byte[0];
//            byte[] hashLastByte = new byte[0];
//            byte[] preRotationByte = new byte[0];
//            byte[] resultByte = new byte[0];
//            byte[] concatedDataByte = new byte[0];

            boolean isHex = false;

            Log.d(TAG,"Start Encrypt");
            Log.d(TAG,"Check Key");
            if(Utils.isHexString(key.getText().toString())){
                isHex = true;
                try {
                    keyByte = Utils.stringHexToByte(key.getText().toString());
                } catch (DecoderException e) {
                    e.printStackTrace();
                }
            }else {
                isHex = true;
                keyByte = key.getText().toString().getBytes();
            }

            if ((keyByte.length == 16) | (keyByte.length ==24) | (keyByte.length ==32)){
                Log.d(TAG,"VALID KEY LENGTH");
            }else{
                Log.d(TAG,"INVALID KEY LENGTH");
            }

            Log.d(TAG,"Check Plaintext");
            if(Utils.isHexString(dataText.getText().toString())){
                try {
                    dataByte = Utils.stringHexToByte(dataText.getText().toString());
                } catch (DecoderException e) {
                    e.printStackTrace();
                }
            }else {
                dataByte = dataText.getText().toString().getBytes();
            }

            MyAlgo myAlgo = new MyAlgo(keyByte);
            try {
                myAlgo.encrypt(dataByte);
            } catch (InvalidCipherTextException e) {
                e.printStackTrace();
            } catch (DecoderException e) {
                e.printStackTrace();
            }

            hash.setText(Utils.byteToStringHex(myAlgo.getHashByte()));
//            Log.d(TAG,Utils.byteToStringHex(myAlgo.getHashByte()));
            hashInt.setText(Utils.byteToStringHex(myAlgo.getHashLastByte())+" ("+new BigInteger(1, myAlgo.getHashLastByte()) +")");
//            Log.d(TAG,Utils.byteToStringHex(myAlgo.getHashLastByte())+" ("+new BigInteger(1, myAlgo.getHashLastByte()) +")");
            if (isHex){
                resultPreRotation.setText(myAlgo.getPreRotationHex());
            }else {
                try {
                    resultPreRotation.setText(new String(
                            Base64.encodeBase64(
                                    Utils.stringHexToByte(
                                            myAlgo.getPreRotationHex()
                                    )
                            )
                    ));
                } catch (DecoderException e) {
                    e.printStackTrace();
                }
            }

            if (isHex) {
                result.setText(Utils.byteToStringHex(myAlgo.getConcatedDataByte()));
            } else {
                result.setText(new String(
                        Base64.encodeBase64(myAlgo.getConcatedDataByte())
                ));
            }

            System.out.println("Data\t\t\t\t\t: " + Utils.byteToStringHex(myAlgo.getDataByte()));
            System.out.println("Hash\t\t\t\t\t\t: " + Utils.byteToStringHex(myAlgo.getHashByte()));
            System.out.println("Last 32-bit\t\t\t\t\t: " + Utils.byteToStringHex(myAlgo.getHashLastByte()));
            System.out.println("Last 32-bit (Bigint Hex)\t: " + new BigInteger(myAlgo.getHashLastByte()).toString(16));
            System.out.println("Last 32-bit (int)\t\t\t: " + new BigInteger(myAlgo.getHashLastByte()));
            System.out.println("Last 32-bit (int)\t\t\t: " + new BigInteger(myAlgo.getHashLastByte()));
            System.out.println("Encrypted\t\t\t\t\t: " + myAlgo.getPreRotationHex());
            System.out.println("Left Rotated Byte\t\t\t: " + Utils.byteToStringHex(myAlgo.getLeftRotatedByte()));
            System.out.println("Left Rotated BigInt\t\t\t: " + myAlgo.getLeftRotated().toString(16));
            System.out.println("Concated Utils\t\t\t\t: " + Utils.byteToStringHex(myAlgo.getConcatedDataByte()));
            System.out.println("Concated BigInt\t\t\t\t: " + new BigInteger(myAlgo.getConcatedDataByte()).toString(16));





//            Log.d(TAG,"Hashing");
//            HashSHA3 hashSHA3 = new HashSHA3(256);
//            hashByte = hashSHA3.hashByte(dataByte, null);
//            hashLastByte = Utils.get32BitOf256Bit(hashByte);
//
//            try {
//                hash.setText(Utils.byteToStringHex(hashByte));
//                hashInt.setText(Utils.byteToStringHex(hashLastByte)+" ("+new BigInteger(hashLastByte).toString() +")");
//                Log.d(TAG,Utils.byteToStringHex(hashByte));
//                Log.d(TAG,Utils.byteToStringHex(hashLastByte)+" ("+new BigInteger(hashLastByte).toString() +")");
//            } catch (DecoderException e) {
//                e.printStackTrace();
//            }
//
//            Log.d(TAG,"Encrypting");
//            AESUtil aesUtil = null;
//
//            try {
//                aesUtil = new AESUtil(keyByte);
//                aesUtil.InitCiphers();
//                preRotationByte = aesUtil.encrypt(dataByte);
//                if (isHex){
//                    resultPreRotation.setText(Utils.byteToStringHex(preRotationByte));
//                }else {
//                    resultPreRotation.setText(new String(
//                            Base64.encodeBase64(preRotationByte)
//                    ));
//                }
//            } catch (DecoderException e) {
//                e.printStackTrace();
//            } catch (InvalidCipherTextException e) {
//                e.printStackTrace();
//            }
//
//            Log.d(TAG,"Rotating");
//            int numberOfRotation = new BigInteger(hashLastByte).intValue();
//            ShiftBytes shiftBytes = new ShiftBytes();
//            BigInteger leftRotated = shiftBytes.cyclicLeftShift(new BigInteger(preRotationByte), (preRotationByte.length*8), numberOfRotation);
//            byte[] leftRotatedByte = leftRotated.toByteArray();
//            concatedDataByte = new byte[leftRotatedByte.length + hashByte.length];
//            System.arraycopy(hashByte, 0, concatedDataByte, 0, hashByte.length);
//            System.arraycopy(leftRotatedByte, 0, concatedDataByte, hashByte.length, leftRotatedByte.length);
//
//            try {
//                Log.d(TAG, "Hash: "+Integer.toString(hashByte.length));
//                Log.d(TAG, "Hash: "+Utils.byteToStringHex(hashByte));
//                Log.d(TAG, "LeftRotated: "+Integer.toString(leftRotatedByte.length));
//                Log.d(TAG, "LeftRotated: "+Utils.byteToStringHex(leftRotatedByte));
//                Log.d(TAG, "Cipher: "+Integer.toString(concatedDataByte.length));
//                Log.d(TAG, "Cipher: "+Utils.byteToStringHex(concatedDataByte));
//            } catch (DecoderException e) {
//                e.printStackTrace();
//            }
//
//
//
//
//
//
//            try {
//                if (isHex) {
//                    result.setText(Utils.byteToStringHex(concatedDataByte));
//                } else {
//                    result.setText(new String(
//                            Base64.encodeBase64(concatedDataByte)
//                    ));
//                }
//            }catch (DecoderException e){
//                e.printStackTrace();
//            }
        });

        decryptButton.setOnClickListener(view -> {
            byte[] keyByte = new byte[0];
//            byte[] dataByte = new byte[0];
            byte[] concatedDataByte = new byte[0];
//            byte[] preRotationByte = new byte[0];
//            byte[] hashAttachedByte = new byte[32];
//            byte[] hashLastByte = new byte[0];
//            byte[] resultByte = new byte[0];

            boolean isHex = false;

            Log.d(TAG,"Start Decrypt");
            Log.d(TAG,"Check Key");
            if(Utils.isHexString(key.getText().toString())){
                isHex = true;
                try {
                    keyByte = Utils.stringHexToByte(key.getText().toString());
                } catch (DecoderException e) {
                    e.printStackTrace();
                }
            }else {
                isHex = true;
                keyByte = key.getText().toString().getBytes();
            }

            if ((keyByte.length == 16) | (keyByte.length ==24) | (keyByte.length ==32)){
                Log.d(TAG,"VALID KEY LENGTH");
            }else{
                Log.d(TAG,"INVALID KEY LENGTH");
            }

            Log.d(TAG,"Check Ciphertext");
            if(Utils.isHexString(dataText.getText().toString())){
                try {
                    concatedDataByte = Utils.stringHexToByte(dataText.getText().toString());
                } catch (DecoderException e) {
                    e.printStackTrace();
                }
            }else {
                concatedDataByte = Base64.decodeBase64(dataText.getText().toString());
            }

            MyAlgo myAlgoDecrypt = new MyAlgo(keyByte);
            try {
                myAlgoDecrypt.decrypt(concatedDataByte);
            } catch (InvalidCipherTextException e) {
                e.printStackTrace();
            } catch (DecoderException e) {
                e.printStackTrace();
            }
            System.out.println("Concated Utils\t\t\t\t: " + Utils.byteToStringHex(myAlgoDecrypt.getConcatedDataByte()));
            System.out.println("Concated BigInt\t\t\t\t: " + new BigInteger(myAlgoDecrypt.getConcatedDataByte()).toString(16));
            System.out.println("Left Rotated Byte\t\t\t: " + Utils.byteToStringHex(myAlgoDecrypt.getLeftRotatedByte()));
            System.out.println("Left Rotated BigInt\t\t\t: " + myAlgoDecrypt.getLeftRotated().toString(16));
            System.out.println("Encrypted\t\t\t\t\t: " + myAlgoDecrypt.getPreRotationHex());
            System.out.println("Hash Attached\t\t\t\t: " + Utils.byteToStringHex(myAlgoDecrypt.getHashAttachedByte()));
            System.out.println("Last 32-bit\t\t\t\t\t: " + Utils.byteToStringHex(myAlgoDecrypt.getHashLastByte()));
            System.out.println("Last 32-bit (Bigint Hex)\t: " + new BigInteger(myAlgoDecrypt.getHashLastByte()).toString(16));
            System.out.println("Last 32-bit (int)\t\t\t: " + new BigInteger(myAlgoDecrypt.getHashLastByte()));
            System.out.println("Last 32-bit (int)\t\t\t: " + new BigInteger(myAlgoDecrypt.getHashLastByte()));
            System.out.println("Decrypted\t\t\t\t\t: " + Utils.byteToStringHex(myAlgoDecrypt.getDataByte()));
            System.out.println("Hash\t\t\t\t\t\t: " + Utils.byteToStringHex(myAlgoDecrypt.getHashByte()));

            hash.setText(Utils.byteToStringHex(myAlgoDecrypt.getHashByte()));
            hashInt.setText(Utils.byteToStringHex(myAlgoDecrypt.getHashLastByte())+" ("+new BigInteger(1, myAlgoDecrypt.getHashLastByte()) +")");
            if (isHex){
                resultPreRotation.setText(myAlgoDecrypt.getPreRotationHex());
            }else {
                try {
                    resultPreRotation.setText(new String(
                            Base64.encodeBase64(
                                    Utils.stringHexToByte(
                                            myAlgoDecrypt.getPreRotationHex()
                                    )
                            )
                    ));
                } catch (DecoderException e) {
                    e.printStackTrace();
                }
            }

            if (isHex) {
                result.setText(Utils.byteToStringHex(myAlgoDecrypt.getDataByte()));
            } else {
                result.setText(new String(
                        Base64.encodeBase64(myAlgoDecrypt.getDataByte())
                ));
            }


//
//            Log.d(TAG, "Cipher: "+Integer.toString(concatedDataByte.length));
//
//            preRotationByte = new byte[concatedDataByte.length-32];
//            System.arraycopy(concatedDataByte, 0, hashAttachedByte, 0, 32);
//            System.arraycopy(concatedDataByte, 32, preRotationByte, 0, concatedDataByte.length-32);
//            Log.d(TAG, "Hash: "+ hashAttachedByte.length);
//            Log.d(TAG, "LeftRotated: "+ preRotationByte.length);
//            try {
//                Log.d(TAG, Utils.byteToStringHex(hashAttachedByte));
//                Log.d(TAG, Utils.byteToStringHex(preRotationByte));
//
//            } catch (DecoderException e) {
//                e.printStackTrace();
//            }
//
//
//
//
//            Log.d(TAG,"Rotating");
//            hashLastByte = new byte[6];
//            hashLastByte = Utils.get32BitOf256Bit(hashAttachedByte);
//            int numberOfRotation = new BigInteger(hashLastByte).intValue();
//            ShiftBytes shiftBytes = new ShiftBytes();
//            BigInteger rightRotated = shiftBytes.cyclicRightShift(new BigInteger(preRotationByte), (preRotationByte.length*8), numberOfRotation);
//            dataByte = rightRotated.toByteArray();
//            Log.d(TAG, "Right Rotataed");
//            try {
//                Log.d(TAG, Utils.byteToStringHex(dataByte));
//            } catch (DecoderException e) {
//                e.printStackTrace();
//            }
////
//            Log.d(TAG,"Decrypting");
//            AESUtil aesUtil = null;
////
//            try {
//                aesUtil = new AESUtil(keyByte);
//                aesUtil.InitCiphers();
//                resultByte = aesUtil.decrypt(dataByte);
//                if (isHex){
//                    resultPreRotation.setText(Utils.byteToStringHex(resultByte));
//                }else {
//                    resultPreRotation.setText(new String(
//                            Base64.encodeBase64(resultByte)
//                    ));
//                }
//            } catch (DecoderException e) {
//                e.printStackTrace();
//            } catch (InvalidCipherTextException e) {
//                e.printStackTrace();
//            }


            //9dc2c84a37850c11699818605f47958c
            //48656c6c6f61
            //00fffffffffffffffffffa161c820fa593
            //5df9f1b761895b11d2ac02d4312dcb25872faa2bf811eb0b34c5b5b263e40cb600fffffffffffffffffffa161c820fa593
//            D/EncryptText: Hash: 32
//            LeftRotated: 17
//            Cipher: 49
            //5df9f1b761895b11d2ac02d4312dcb25872faa2bf811eb0b34c5b5b263e40cb600fffffffffffffffffffa161c820fa593

        });
        return views;
    }
}