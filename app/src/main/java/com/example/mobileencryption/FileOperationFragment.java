package com.example.mobileencryption;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mobileencryption.crypto.MyAlgo;
import com.example.mobileencryption.utils.FileUtils;
import com.example.mobileencryption.utils.Utils;

import org.apache.commons.codec.DecoderException;
import org.bouncycastle.crypto.InvalidCipherTextException;

import java.io.File;
import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FileOperationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FileOperationFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FileOperationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FileOperationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FileOperationFragment newInstance(String param1, String param2) {
        FileOperationFragment fragment = new FileOperationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private Button buttonBrowse, encryptFileButton, decryptFileButton;
    private EditText editTextPath, keyFile, resultFile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_file_operation, container, false);

        View rootView = inflater.inflate(R.layout.fragment_file_operation, container, false);

        this.editTextPath = (EditText) rootView.findViewById(R.id.editText_path);
        this.buttonBrowse = (Button) rootView.findViewById(R.id.button_browse);
        this.encryptFileButton = rootView.findViewById(R.id.encryptFileButton);
        this.decryptFileButton = rootView.findViewById(R.id.decryptFileButton);
        this.resultFile = rootView.findViewById(R.id.resultFile);
        this.keyFile = rootView.findViewById(R.id.keyFile);


        this.buttonBrowse.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                askPermissionAndBrowseFile();
            }

        });

        this.encryptFileButton.setOnClickListener( view -> {
                encryptFile();
            }
        );

        this.decryptFileButton.setOnClickListener( view -> {
                decryptFile();
            }
        );
        return rootView;
    }

    private static final int MY_REQUEST_CODE_PERMISSION = 1000;
    private static final int MY_RESULT_CODE_FILECHOOSER = 2000;



    private static final String LOG_TAG = "AndroidExample";

    private void askPermissionAndBrowseFile()  {
        // With Android Level >= 23, you have to ask the user
        // for permission to access External Storage.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) { // Level 23

            // Check if we have Call permission
            int permisson = ActivityCompat.checkSelfPermission(this.getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (permisson != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_REQUEST_CODE_PERMISSION
                );
                return;
            }
        }
        this.doBrowseFile();
    }

    private void doBrowseFile()  {
        Intent chooseFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFileIntent.setType("*/*");
        // Only return URIs that can be opened with ContentResolver
        chooseFileIntent.addCategory(Intent.CATEGORY_OPENABLE);

        chooseFileIntent = Intent.createChooser(chooseFileIntent, "Choose a file");
        startActivityForResult(chooseFileIntent, MY_RESULT_CODE_FILECHOOSER);
    }

    // When you have the request results
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        switch (requestCode) {
            case MY_REQUEST_CODE_PERMISSION: {

                // Note: If request is cancelled, the result arrays are empty.
                // Permissions granted (CALL_PHONE).
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i( LOG_TAG,"Permission granted!");
                    Toast.makeText(this.getContext(), "Permission granted!", Toast.LENGTH_SHORT).show();

                    this.doBrowseFile();
                }
                // Cancelled or denied.
                else {
                    Log.i(LOG_TAG,"Permission denied!");
                    Toast.makeText(this.getContext(), "Permission denied!", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case MY_RESULT_CODE_FILECHOOSER:
                if (resultCode == Activity.RESULT_OK ) {
                    if(data != null)  {
                        Uri fileUri = data.getData();
                        Log.i(LOG_TAG, "Uri: " + fileUri);

                        String filePath = null;
                        try {
                            filePath = FileUtils.getPath(this.getContext(),fileUri);
                        } catch (Exception e) {
                            Log.e(LOG_TAG,"Error: " + e);
                            Toast.makeText(this.getContext(), "Error: " + e, Toast.LENGTH_SHORT).show();
                        }
                        this.editTextPath.setText(filePath);
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getPath()  {
        return this.editTextPath.getText().toString();
    }

    private void encryptFile(){
        Log.d(LOG_TAG,"Encrypt Process");
        String pathFileInput = editTextPath.getText().toString();
        String pathFileOutput = pathFileInput+".enc";

        byte[] keyByte = new byte[0];
        byte[] dataByte = new byte[0];
        boolean isHex = false;

        try {
            dataByte = org.apache.commons.io.FileUtils.readFileToByteArray(
                    new File(pathFileInput)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(Utils.isHexString(keyFile.getText().toString())){
            isHex = true;
            try {
                keyByte = Utils.stringHexToByte(keyFile.getText().toString());
            } catch (DecoderException e) {
                e.printStackTrace();
            }
        }else {
            isHex = true;
            keyByte = keyFile.getText().toString().getBytes();
        }

        if ((keyByte.length == 16) | (keyByte.length ==24) | (keyByte.length ==32)){
            Log.d(LOG_TAG,"VALID KEY LENGTH");
        }else{
            Log.d(LOG_TAG,"INVALID KEY LENGTH");
        }

        MyAlgo myAlgo = new MyAlgo(keyByte);
        try {
            myAlgo.encrypt(dataByte);
        } catch (InvalidCipherTextException e) {
            e.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }



        try {
            org.apache.commons.io.FileUtils.writeByteArrayToFile(
                    new File(pathFileOutput),
                    myAlgo.getConcatedDataByte()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        resultFile.setText(
                pathFileOutput
        );

        Log.d(LOG_TAG,"File Input: "+pathFileInput);
        Log.d(LOG_TAG,"File Output: "+pathFileOutput);
//        Log.d(LOG_TAG,"Byte: "+new String(bytes));
    }

    private void decryptFile(){
        Log.d(LOG_TAG,"Decrypt Process");
        String pathFileInput = editTextPath.getText().toString();
        String pathFileOutput = pathFileInput.replace(".enc", "");
        byte[] keyByte = new byte[0];
        byte[] concatedDataByte = new byte[0];
        boolean isHex = false;

//        byte[] bytes = new byte[0];
        try {
            concatedDataByte = org.apache.commons.io.FileUtils.readFileToByteArray(
                    new File(pathFileInput)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(Utils.isHexString(keyFile.getText().toString())){
            isHex = true;
            try {
                keyByte = Utils.stringHexToByte(keyFile.getText().toString());
            } catch (DecoderException e) {
                e.printStackTrace();
            }
        }else {
            isHex = true;
            keyByte = keyFile.getText().toString().getBytes();
        }

        if ((keyByte.length == 16) | (keyByte.length ==24) | (keyByte.length ==32)){
            Log.d(LOG_TAG,"VALID KEY LENGTH");
        }else{
            Log.d(LOG_TAG,"INVALID KEY LENGTH");
        }

        MyAlgo myAlgoDecrypt = new MyAlgo(keyByte);
        try {
            myAlgoDecrypt.decrypt(concatedDataByte);
        } catch (InvalidCipherTextException e) {
            e.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }


        try {
            org.apache.commons.io.FileUtils.writeByteArrayToFile(
                    new File(pathFileOutput),
                    myAlgoDecrypt.getDataByte()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        resultFile.setText(
                pathFileOutput
        );
        Log.d(LOG_TAG,"File Input: "+pathFileInput);
        Log.d(LOG_TAG,"File Output: "+pathFileOutput);
//        Log.d(LOG_TAG,"Byte: "+new String(bytes));
    }






//    int requestCode = 1;
//
//
//    public void onActivityResult(int requestCode, int resultCode, Intent data){
//        super.onActivityResult(requestCode, resultCode, data);
//        Context context = getContext().getApplicationContext();
//        if(requestCode == requestCode && resultCode == Activity.RESULT_OK)
//        {
//            if (data == null)
//            {
//                return;
//            }
//            Uri uri = data.getData();
//            Toast.makeText(context, uri.getPath(), Toast.LENGTH_SHORT).show();
//
//
//        }
//
//    }
//
//    public void openFileChooser(android.app.Fragment fragment){
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("*/*");
//        startActivityForResult(intent, requestCode);
//    }
//
//    public static final int REQUEST_CODE_OPEN_OPML_FILE = 1234;
//
//    /**
//     * Opens a file chooser
//     * @param fragment
//     */
//    public static void openOpmlFileChooser(Fragment fragment) {
//        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("text/*");
//        fragment.startActivityForResult(intent, REQUEST_CODE_OPEN_OPML_FILE);
//    }
}