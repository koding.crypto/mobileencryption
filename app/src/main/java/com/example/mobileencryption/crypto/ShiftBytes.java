package com.example.mobileencryption.crypto;

import com.example.mobileencryption.utils.Utils;
import org.apache.commons.codec.DecoderException;

import java.math.BigInteger;

public class ShiftBytes {
//        /* In n<<d, last d bits are 0.
//       To put first 3 bits of n at
//       last, do bitwise or of n<<d with
//       n >>(INT_BITS - d)
//
//       n = value
//       d = shift
//       */

    public BigInteger cyclicLeftShift(BigInteger n, int L, int k) throws DecoderException {
//        System.out.println("Length");
//        System.out.println(n.bitLength());
//        System.out.println("Input");
//        System.out.println(Utils.byteToStringHex(n.toByteArray()));
//        System.out.println("First");
//        System.out.println(Utils.byteToStringHex(n.shiftLeft(k).toByteArray()));
//        System.out.println("Second");
//        System.out.println(Utils.byteToStringHex(n.shiftRight(L - k).toByteArray()));
//        System.out.println("First or Second");
//        System.out.println(Utils.byteToStringHex(
//                (n.shiftLeft(k)
//                        .or(n.shiftRight(L - k))).toByteArray()
//        ));
//        System.out.println("3th");
//        System.out.println(Utils.byteToStringHex(allOnes(L).toByteArray()));
//
//        return n.shiftLeft(k)
//                .or(n.shiftRight(L - k))
//                .and(allOnes(L));
        if (k > L) {
            k = BigInteger.valueOf(k).mod(
                    BigInteger.valueOf(L)).intValue();
        }


        return n.shiftLeft(k)
                .or(n.shiftRight(L - k))
                .and(allOnes(L))
                ;

//        BigInteger topBits = n.shiftRight(L - k);
//        System.out.println(Utils.byteToStringHex(topBits.toByteArray()));
//        BigInteger mask = BigInteger.ONE.shiftLeft(L).subtract(BigInteger.ONE);
////        System.out.println(Utils.byteToStringHex(mask.toByteArray()));
//        return n.shiftLeft(k).or(topBits).and(mask);
    }

    public BigInteger cyclicRightShift(BigInteger n, int L, int k) {
        if (k > L) {
            k = BigInteger.valueOf(k).mod(
                    BigInteger.valueOf(L)).intValue();
        }
        return n.shiftRight(k)
                .or(n.shiftLeft(L - k))
                .and(allOnes(L))
                ;
    }

    BigInteger allOnes(int L) {
        return BigInteger.ZERO
                .setBit(L)
                .subtract(BigInteger.ONE);
    }

    public BigInteger rotateLeft(BigInteger value, int shift, int bitSize)
    {
        // Note: shift must be positive, if necessary add checks.

        BigInteger topBits = value.shiftRight(bitSize - shift);
        BigInteger mask = BigInteger.ONE.shiftLeft(bitSize).subtract(BigInteger.ONE);
        return value.shiftLeft(shift).or(topBits).and(mask);
    }
}
